#include <cstdlib>
#include <iostream>
#include "InsertionSort.h"

using namespace std;

InsertionSort::InsertionSort() {
    tab = new int[20];
    for(int i=0;i<20;i++){
        tab[i] = rand() % 20;
    }
    length = 20;
}

InsertionSort::InsertionSort(int tab[], int len) {
    this->tab = tab;
    length = len;
}

void InsertionSort::exec() {
    int j = 1;
    while(j < length) {
        int currVal = tab[j];

        int i = j - 1;
        while(i>-1 && tab[i]>currVal) {
            tab[i+1] = tab[i];
            i--;
        }
        tab[i+1] = currVal;
        j++;
    }
}

void InsertionSort::printTab() {
    for(int i=0;i<length;i++){
        cout << tab[i] << endl;
    }
}
