#include <iostream>
#include <random>
#include <chrono>
#include "InsertionSort.h"

using namespace std;

void checkTab(int* tab, int len) {
    for(int i=1;i<len;i++){
        if(tab[i-1] > tab[i]) {
            cout << "Array is not sorted correctly" << endl;
            break;
        }
    }
}

void showTab(int* tab, int len) {
    cout << "Tab: ";
    for(int i=0;i<len;i++){
        cout << tab[i] << " ";
    }
    cout << endl << endl;
}

void findDups(int* tab, int len) {
    int dupsLen = len/2 +1;
    int dups[dupsLen];
    int last = -1;

    for(int i=1;i<len;i++) {
        if(tab[i - 1] == tab[i]) {
            if(last == -1) {
                dups[++last] = tab[i];
            } else {
                if(dups[last] != tab[i]) {
                    dups[++last] = tab[i];
                }
            }
        }
    }

    cout << "Duplicates: ";

    for(int i=0;i<=last;i++) {
        cout << dups[i] << " ";
    }

    cout << endl << endl;
}

int main() {
    mt19937 mt(chrono::steady_clock::now().time_since_epoch().count());
    uniform_real_distribution<double> dist(1.0, 500.0);

    int len = 10;
    int* tab = new int[len];

    for(int i=0;i<len;i++){
        tab[i] = dist(mt);
    }

    InsertionSort object1 = InsertionSort(tab, len);
    object1.exec();
    checkTab(tab, len);
}