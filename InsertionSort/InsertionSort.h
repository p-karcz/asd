#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

class InsertionSort {
private:
    int* tab = nullptr;
    int length = 0;

public:
    InsertionSort();
    InsertionSort(int*, int);
    void exec();
    void printTab();
};

#endif