#ifndef ASD_MERGESORT_H
#define ASD_MERGESORT_H

class MergeSort {

public:
    void sort(int* tab, int left, int right);
    void merge(int* tab, int left, int mid, int right);
};

#endif //ASD_MERGESORT_H
