#include "MergeSort.h"

void MergeSort::sort(int tab[], int left, int right) {
    if(left < right) {
        int mid = (left + right) / 2;
        sort(tab,left,mid);
        sort(tab,mid + 1,right);
        merge(tab,left,mid,right);
    }
}

void MergeSort::merge(int tab[], int left, int mid, int right) {
    int lenLeft = mid - left + 1;
    int lenRight = right - mid;

    int leftTab[lenLeft];
    int rightTab[lenRight];

    for(int i=0;i<lenLeft;i++) {
        leftTab[i] = tab[left + i];
    }
    for(int i=0;i<lenRight;i++) {
        rightTab[i] = tab[mid + 1 + i];
    }

    int tabIndex = left;
    int leftIndex = 0;
    int rightIndex = 0;

    while(tabIndex <= right) {
        if(leftIndex >= lenLeft) {
            tab[tabIndex] = rightTab[rightIndex];
            rightIndex++;
        } else if(rightIndex >= lenRight) {
            tab[tabIndex] = leftTab[leftIndex];
            leftIndex++;
        } else {
            if(leftTab[leftIndex] <= rightTab[rightIndex]) {
                tab[tabIndex] = leftTab[leftIndex];
                leftIndex++;
            } else {
                tab[tabIndex] = rightTab[rightIndex];
                rightIndex++;
            }
        }
        tabIndex++;
    }
}