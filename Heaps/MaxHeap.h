#ifndef ASD_MAXHEAP_H
#define ASD_MAXHEAP_H

class MaxHeap {
private:
    int size;
    int unsortedSize;
    int* tab;
public:
    MaxHeap(int* tab, int size);
    int goDown(int actualIndex);
    int ancestor(int index);
    int leftChild(int index);
    int rightChild(int index);
    void heapify(int index);
    void bottomUpHeapify(int index);
    int* buildHeap();
    int* heapSort();
    int* insert(int x);
    int extractMax();
};

#endif //ASD_MAXHEAP_H
