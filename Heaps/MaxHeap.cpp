#include "MaxHeap.h"
#include <iostream>

using namespace std;

MaxHeap::MaxHeap(int *tab, int size) {
    this->tab = tab;
    this->size = size;
    unsortedSize = size;
}

void MaxHeap::heapify(int index) {
    int temp1;
    int temp2;
    int lastIndex = goDown(index);

    while(tab[lastIndex] < tab[index]) {
        lastIndex = ancestor(lastIndex);
    }

    temp1 = tab[lastIndex];
    tab[lastIndex] = tab[index];

    while(lastIndex > index) {
        lastIndex = ancestor(lastIndex);
        temp2 = tab[lastIndex];
        tab[lastIndex] = temp1;
        temp1 = temp2;
    }
}

void MaxHeap::bottomUpHeapify(int index) {
    int temp;
    int ancestorIndex = ancestor(index);

    while(index != 0 && tab[index] > tab[ancestorIndex]) {
        temp = tab[index];
        tab[index] = tab[ancestorIndex];
        tab[ancestorIndex] = temp;
        index = ancestorIndex;

        ancestorIndex = ancestor(index);
    }
}

int MaxHeap::goDown(int actualIndex) {
    int left;
    int right;

    while(actualIndex <= (unsortedSize / 2) - 1) {
        left = leftChild(actualIndex);
        right = rightChild(actualIndex);

        if(left != -1 && right != -1) {
            if(tab[left] >= tab[right]) {
                actualIndex = left;
            } else {
                actualIndex = right;
            }
        } else if(left != -1) {
            actualIndex = left;
        } else {
            actualIndex = right;
        }
    }
    return actualIndex;
}

int* MaxHeap::buildHeap() {
    for(int i= (unsortedSize / 2) - 1; i >= 0; i--) {
        heapify(i);
    }

    return tab;
}

int* MaxHeap::insert(int x) {
    int* newTab = new int[++size];

    for(int i=0;i<size - 1;i++) {
        newTab[i] = tab[i];
    }

    newTab[size - 1] = x;

    delete tab;
    tab = newTab;

    bottomUpHeapify(size - 1);

    unsortedSize = size;

    return tab;
}

int MaxHeap::ancestor(int index) {
    return (index-1) >> 1;
}

int MaxHeap::leftChild(int index) {
    int leftChildIndex = (index << 1) + 1;

    if(leftChildIndex < unsortedSize) {
        return leftChildIndex;
    } else {
        return -1;
    }
}

int MaxHeap::rightChild(int index) {
    int rightChildIndex = (index << 1) + 2;

    if(rightChildIndex < unsortedSize) {
        return rightChildIndex;
    } else {
        return -1;
    }
}

int MaxHeap::extractMax() {
    int maxVal = tab[0];
    tab[0] = tab[--unsortedSize];
    tab[unsortedSize] = maxVal;
    heapify(0);
    return maxVal;
}

int* MaxHeap::heapSort() {
    while(unsortedSize > 0) {
        extractMax();
    }

    return tab;
}