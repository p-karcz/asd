#include "Bst.h"

Bst::Bst() {
    root = nullptr;
}

Bst::Bst(int root) {
    this->root = new Node(root, nullptr);
}

Node* Bst::getRoot() {
    return root;
}

Node* Bst::addVal(int val) {
    if(root == nullptr) {
        Node* newNode = new Node(val, nullptr);
        root = newNode;
        return newNode;
    } else {
        Node* currNode = root;
        while(true) {
            if (val < currNode->getValue()) {
                if (currNode->getLeftChild() == nullptr) {
                    Node* newNode = new Node(val, currNode);
                    currNode->addLeftChild(newNode);
                    return newNode;
                } else {
                    currNode = currNode->getLeftChild();
                }
            } else {
                if (currNode->getRightChild() == nullptr) {
                    Node* newNode = new Node(val, currNode);
                    currNode->addRightChild(newNode);
                    return newNode;
                } else {
                    currNode = currNode->getRightChild();
                }
            }
        }
    }
}

Node* Bst::searchKey(int key) {
    Node* currNode = root;
    while(currNode != nullptr){
        if(currNode->getValue() != key) {
            if(key < currNode->getValue()) {
                currNode = currNode->getLeftChild();
            } else {
                currNode = currNode->getRightChild();
            }
        } else {
            return currNode;
        }
    }
    return nullptr;
}

// returns pointer to the parent of the deleted node (if root was deleted then it returns root)
Node* Bst::deleteKey(int key) {
    bool left = false;
    Node* keyNodeParent = nullptr;
    Node* keyNode = root;

    if(root == nullptr) {
        return nullptr;
    }

    while(keyNode->getValue() != key) {
        if(key < keyNode->getValue()) {
            keyNode = keyNode->getLeftChild();
            left = true;
        } else {
            keyNode = keyNode->getRightChild();
            left = false;
        }

        if(keyNode == nullptr) {
            return nullptr;
        }
    }

    keyNodeParent = keyNode->getParent();

    cout << endl << "Left: " << left << endl;
    cout << "KeyNodeVal: " << keyNode->getValue() << endl;
    if(keyNodeParent != nullptr)
    cout << "KeyNodeParentVal: " << keyNodeParent->getValue() << endl;

    Node* tempNode = nullptr;
    Node* tempNodeParent = nullptr;

    if(keyNodeParent == nullptr) {
        if(keyNode->getLeftChild() == nullptr) {
            if(keyNode->getRightChild() != nullptr) {
                keyNode->getRightChild()->addParent(nullptr);
            }
            root = keyNode->getRightChild();
            delete(keyNode);
            return root;
        } else if(keyNode->getRightChild() == nullptr) {
            if(keyNode->getLeftChild() != nullptr) {
                keyNode->getLeftChild()->addParent(nullptr);
            }
            root = keyNode->getLeftChild();
            delete(keyNode);
            return root;
        } else {
            tempNode = keyNode->getRightChild();
            if(tempNode->getLeftChild() == nullptr) {
                keyNode->setValue(tempNode->getValue());
                keyNode->addRightChild(tempNode->getRightChild());
                if(tempNode->getRightChild() != nullptr) {
                    tempNode->getRightChild()->addParent(keyNode);
                }
                delete(tempNode);
            } else {
                while(tempNode->getLeftChild() != nullptr) {
                    tempNode = tempNode->getLeftChild();
                }
                keyNode->setValue(tempNode->getValue());
                tempNodeParent = tempNode->getParent();
                tempNodeParent->addLeftChild(tempNode->getRightChild());
                if(tempNode -> getRightChild() != nullptr) {
                    tempNode->getRightChild()->addParent(tempNodeParent);
                }
                delete(tempNode);
            }
            return root;
        }
    } else if(left) {
        if(keyNode->getLeftChild() == nullptr) {
            if(keyNode->getRightChild() != nullptr) {
                keyNode->getRightChild()->addParent(keyNodeParent);
            }
            keyNodeParent->addLeftChild(keyNode->getRightChild());
            delete(keyNode);
            tempNodeParent = keyNodeParent;
        } else if(keyNode->getRightChild() == nullptr) {
            if(keyNode->getLeftChild() != nullptr) {
                keyNode->getLeftChild()->addParent(keyNodeParent);
            }
            keyNodeParent->addLeftChild(keyNode->getLeftChild());
            delete(keyNode);
            tempNodeParent = keyNodeParent;
        } else {
            tempNode = keyNode->getRightChild();
            if(tempNode->getLeftChild() == nullptr) {
                keyNode->setValue(tempNode->getValue());
                keyNode->addRightChild(tempNode->getRightChild());
                if(tempNode -> getRightChild() != nullptr) {
                    keyNode->getRightChild()->addParent(keyNode);
                }
                tempNodeParent = tempNode->getParent();
                delete(tempNode);
            } else {
                tempNode = tempNode->getLeftChild();
                while(tempNode->getLeftChild() != nullptr) {
                    tempNode = tempNode->getLeftChild();
                }
                keyNode->setValue(tempNode->getValue());
                tempNodeParent = tempNode->getParent();
                tempNodeParent->addLeftChild(tempNode->getRightChild());
                if(tempNode->getRightChild() != nullptr) {
                    tempNode->getRightChild()->addParent(tempNodeParent);
                }
                delete(tempNode);
            }
        }
    } else {
        if(keyNode->getLeftChild() == nullptr) {
            keyNodeParent->addRightChild(keyNode->getRightChild());
            if(keyNode->getRightChild() != nullptr) {
                keyNode->getRightChild()->addParent(keyNodeParent);
            }
            tempNodeParent = keyNodeParent;
            delete(keyNode);
        } else if(keyNode->getRightChild() == nullptr) {
            keyNodeParent->addRightChild(keyNode->getLeftChild());
            if(keyNode->getLeftChild() != nullptr) {
                keyNode->getLeftChild()->addParent(keyNodeParent);
            }
            tempNodeParent = keyNodeParent;
            delete(keyNode);
        } else {
            tempNode = keyNode->getRightChild();
            if(tempNode->getLeftChild() == nullptr) {
                keyNode->setValue(tempNode->getValue());
                keyNode->addRightChild(tempNode->getRightChild());
                if(tempNode->getRightChild() != nullptr) {
                    tempNode->getRightChild()->addParent(keyNode);
                }
                tempNodeParent = tempNode->getParent();
                delete(tempNode);
            } else {
                tempNode = tempNode->getLeftChild();
                while(tempNode->getLeftChild() != nullptr) {
                    tempNode = tempNode->getLeftChild();
                }
                tempNodeParent = tempNode->getParent();
                keyNode->setValue(tempNode->getValue());
                tempNodeParent->addLeftChild(tempNode->getRightChild());
                if(tempNode->getRightChild() != nullptr) {
                    tempNode->getRightChild()->addParent(tempNodeParent);
                }
                delete(tempNode);
            }
        }
    }
    return tempNodeParent;
}

Node * Bst::rightRotation(Node *nodeToRotate) {
    Node* x = nodeToRotate->getLeftChild();
    Node* y = x->getRightChild();

    Node* parentNode = nodeToRotate->getParent();

    if(parentNode != nullptr) {
        if(parentNode->getLeftChild() == nodeToRotate) {
            parentNode->addLeftChild(x);
        } else {
            parentNode->addRightChild(x);
        }
    } else {
        root = x;
    }

    x->addRightChild(nodeToRotate);
    x->addParent(nodeToRotate->getParent());
    nodeToRotate->addParent(x);
    nodeToRotate->addLeftChild(y);
    if(y != nullptr) {
        y->addParent(nodeToRotate);
    }

    return x;
}

Node * Bst::leftRotation(Node *nodeToRotate) {
    Node* x = nodeToRotate->getRightChild();
    Node* y = x->getLeftChild();

    Node* parentNode = nodeToRotate->getParent();

    if(parentNode != nullptr) {
        if(parentNode->getLeftChild() == nodeToRotate) {
            parentNode->addLeftChild(x);
        } else {
            parentNode->addRightChild(x);
        }
    } else {
        root = x;
    }

    x->addLeftChild(nodeToRotate);
    x->addParent(nodeToRotate->getParent());
    nodeToRotate->addParent(x);
    nodeToRotate->addRightChild(y);
    if(y != nullptr) {
        y->addParent(nodeToRotate);
    }

    return x;
}

void Bst::preorder(Node* currNode) {
    if(currNode != nullptr) {
        cout << currNode->getValue() << " ";
        preorder(currNode->getLeftChild());
        preorder(currNode->getRightChild());
    }
}

void Bst::inorder(Node *currNode) {
    if(currNode != nullptr) {
        inorder(currNode->getLeftChild());
        cout << " Value: " << currNode->getValue() << " Black?: " << currNode->isColoredBlack() << " Height: " << currNode->getHeight() << endl;
        inorder(currNode->getRightChild());
    }
}

void Bst::postorder(Node *currNode) {
    if(currNode != nullptr) {
        postorder(currNode->getLeftChild());
        postorder(currNode->getRightChild());
        cout << currNode->getValue() << " ";
    }
}