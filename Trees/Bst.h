#ifndef ASD_BST_H
#define ASD_BST_H
#include "Node.h"
#include <iostream>

using namespace std;

class Bst {
protected:
    Node* root;
public:
    Bst();
    Bst(int root);
    Node* getRoot();
    virtual Node* addVal(int val);
    Node* searchKey(int key);
    virtual Node* deleteKey(int key);
    Node* rightRotation(Node* nodeToRotate);
    Node* leftRotation(Node* nodeToRotate);
    void preorder(Node* currNode);
    void inorder(Node* currNode);
    void postorder(Node* currNode);
};

#endif
