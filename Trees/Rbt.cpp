#include "Rbt.h"

Rbt::Rbt() {}

Rbt::Rbt(int root): Bst(root){}

Node * Rbt::addVal(int val) {
    Node* node = Bst::addVal(val);
    fixColoring(node);
    return node;
}

Node * Rbt::deleteKey(int key) {
    bool xCreated = false;
    Node* z = searchKey(key);
    Node* x;
    Node* y;

    if (z == nullptr) {
        return nullptr;
    }

    y = z;
    bool y_original_color = y->isColoredBlack();
    if (z->getLeftChild() == nullptr) {
        x = z->getRightChild();
        if(x == nullptr) {
            x = new Node(-1,true,z);
            z->addRightChild(x);
            xCreated = true;
        }
        rbTransplant(z, z->getRightChild());
    } else if (z->getRightChild() == nullptr) {
        x = z->getLeftChild();
        if(x == nullptr) {
            x = new Node(-1,true,z);
            z->addLeftChild(x);
            xCreated = true;
        }
        rbTransplant(z, z->getLeftChild());
    } else {
        y = minimum(z->getRightChild());
        y_original_color = y->isColoredBlack();
        x = y->getRightChild();
        if(x != nullptr) {
            x->addParent(y);
        } else {
            x = new Node(-1,true,y);
            y->addRightChild(x);
            xCreated = true;
        }
        if (y->getParent() != z) {
            rbTransplant(y, y->getRightChild());
            y->addRightChild(z->getRightChild());
            y->getRightChild()->addParent(y);
        }

        rbTransplant(z, y);
        y->addLeftChild(z->getLeftChild());
        y->getLeftChild()->addParent(y);
        y->setColor(z->isColoredBlack());
    }
    delete z;

    if (y_original_color) {
        deleteFix(x);
    }

    if(xCreated) {
        if(x->getParent()->getLeftChild() == x){
            x->getParent()->addLeftChild(nullptr);
        } else {
            x->getParent()->addRightChild(nullptr);
        }

        delete(x);
    }

    if(root != nullptr)
        root->updateHeight();

    return nullptr;
}

void Rbt::deleteFix(Node* node) {
    Node* s;
    while (node != root && node->isColoredBlack()) {
        if (node == node->getParent()->getLeftChild()) {
            s = node->getParent()->getRightChild();

            if(s == nullptr) {
                node = node->getParent();
                continue;
            }

            if (!s->isColoredBlack()) {
                s->setColor(true);
                node->getParent()->setColor(false);
                leftRotation(node->getParent());
                s = node->getParent()->getRightChild();
            }

            if(s->getLeftChild() == nullptr and s->getRightChild() == nullptr) {
                s->setColor(false);
                node = node->getParent();
                continue;
            } else if(s->getLeftChild() == nullptr or s->getRightChild() == nullptr) {
                if(s->getRightChild() == nullptr) {
                    s->getLeftChild()->setColor(true);
                    s->setColor(false);
                    rightRotation(s);
                    s = node->getParent()->getRightChild();
                }

                s->setColor(node->getParent()->isColoredBlack());
                node->getParent()->setColor(true);
                if(s->getRightChild() != nullptr){
                    s->getRightChild()->setColor(true);
                }
                leftRotation(node->getParent());
                node = root;
                continue;
            }

            if (s->getLeftChild()->isColoredBlack() and s->getRightChild()->isColoredBlack()) {
                s->setColor(false);
                node = node->getParent();
            } else {
                if (s->getRightChild()->isColoredBlack()) {
                    s->getLeftChild()->setColor(true);
                    s->setColor(false);
                    rightRotation(s);
                    s = node->getParent()->getRightChild();
                }

                s->setColor(node->getParent()->isColoredBlack());
                node->getParent()->setColor(true);
                s->getRightChild()->setColor(true);
                leftRotation(node->getParent());
                node = root;
            }
        } else {
            s = node->getParent()->getLeftChild();

            if(s == nullptr) {
                node = node->getParent();
                continue;
            }

            if (!s->isColoredBlack()) {
                s->setColor(true);
                node->getParent()->setColor(false);
                rightRotation(node->getParent());
                s = node->getParent()->getLeftChild();
            }

            if(s->getRightChild() == nullptr and s->getLeftChild() == nullptr) {
                s->setColor(false);
                node = node->getParent();
                continue;
            } else if(s->getLeftChild() == nullptr or s->getRightChild() == nullptr) {
                if(s->getLeftChild() == nullptr) {
                    s->getRightChild()->setColor(true);
                    s->setColor(false);
                    leftRotation(s);
                    s = node->getParent()->getLeftChild();
                }

                s->setColor(node->getParent()->isColoredBlack());
                node->getParent()->setColor(true);
                if(s->getLeftChild() != nullptr) {
                    s->getLeftChild()->setColor(true);
                }
                rightRotation(node->getParent());
                node = root;
                continue;
            }

            if (s->getRightChild()->isColoredBlack() && s->getRightChild()->isColoredBlack()) {
                s->setColor(false);
                node = node->getParent();
            } else {
                if (s->getLeftChild()->isColoredBlack()) {
                    s->getRightChild()->setColor(true);
                    s->setColor(false);
                    leftRotation(s);
                    s = node->getParent()->getLeftChild();
                }

                s->setColor(node->getParent()->isColoredBlack());
                node->getParent()->setColor(true);
                s->getLeftChild()->setColor(true);
                rightRotation(node->getParent());
                node = root;
            }
        }
    }
    node->setColor(true);
}

void Rbt::fixColoring(Node *node) {
    Node* parent;
    Node* grandParent;

    while(node != root and !node->isColoredBlack() and !node->getParent()->isColoredBlack()) {
        parent = node->getParent();
        grandParent = parent->getParent();

        if(parent == grandParent->getLeftChild()) {
            Node* uncle = grandParent->getRightChild();

            if(uncle != nullptr and !uncle->isColoredBlack()) {
                grandParent->setColor(false);
                parent->setColor(true);
                uncle->setColor(true);
                node = grandParent;
            } else {
                if(node == parent->getRightChild()) {
                    leftRotation(parent);
                    node = parent;
                    parent = node->getParent();
                }

                rightRotation(grandParent);
                swapColors(parent, grandParent);
                node = parent;
            }
        } else {
            Node* uncle = grandParent->getLeftChild();

            if(uncle != nullptr and !uncle->isColoredBlack()) {
                grandParent->setColor(false);
                parent->setColor(true);
                uncle->setColor(true);
                node = grandParent;
            } else {
                if(node == parent->getLeftChild()) {
                    rightRotation(parent);
                    node = parent;
                    parent = node->getParent();
                }

                leftRotation(grandParent);
                swapColors(parent, grandParent);
                node = parent;
            }
        }
    }

    root->setColor(true);
    root->updateHeight();
}

Node * Rbt::minimum(Node *node) {
    while (node->getLeftChild() != nullptr) {
        node = node->getLeftChild();
    }
    return node;
}

void Rbt::rbTransplant(Node *u, Node *v) {
    if (u->getParent() == nullptr) {
        root = v;
    } else if (u == u->getParent()->getLeftChild()) {
        u->getParent()->addLeftChild(v);
    } else {
        u->getParent()->addRightChild(v);
    }
    if(v != nullptr)
    v->addParent(u->getParent());
}

void Rbt::swapColors(Node *n1, Node *n2) {
    bool temp = n1->isColoredBlack();
    n1->setColor(n2->isColoredBlack());
    n2->setColor(temp);
}