#include "Node.h"
#include <algorithm>

Node::Node() {
    value = -1;
    height = 1;
    colorBlack = false;
    leftChild = nullptr;
    rightChild = nullptr;
    parent = nullptr;
}

Node::Node(int val, Node* p) {
    value = val;
    height = 1;
    colorBlack = false;
    leftChild = nullptr;
    rightChild = nullptr;
    parent = p;
}

Node::Node(int val, int height, Node* p) {
    value = val;
    this->height = height;
    colorBlack = false;
    leftChild = nullptr;
    rightChild = nullptr;
    parent = p;
}

Node::Node(int val, bool isBlack, Node* p) {
    value = val;
    height = 1;
    colorBlack = isBlack;
    leftChild = nullptr;
    rightChild = nullptr;
    parent = p;
}

void Node::setValue(int val) {
    value = val;
}

int Node::getValue() {
    return value;
}

void Node::setHeight(int height) {
    this->height = height;
}

int Node::getHeight() {
    return this->height;
}

int Node::updateHeight() {
    if(leftChild == nullptr and rightChild == nullptr) {
        height = 1;
        return 1;
    } else if(leftChild == nullptr) {
        height = 1 + rightChild->updateHeight();
        return height;
    } else if(rightChild == nullptr) {
        height = 1 + leftChild->updateHeight();
        return height;
    } else {
        height = 1 + std::max(leftChild->updateHeight(), rightChild->updateHeight());
        return height;
    }
}

int Node::getHeightDifference() {
    if(leftChild == nullptr and rightChild == nullptr) {
        return 0;
    } else if(leftChild == nullptr) {
        return -rightChild->getHeight();
    } else if(rightChild == nullptr) {
        return leftChild->getHeight();
    } else {
        return leftChild->getHeight() - rightChild->getHeight();
    }
}

void Node::setColor(bool isBlack) {
    colorBlack = isBlack;
}

bool Node::isColoredBlack() {
    return colorBlack;
}

void Node::addLeftChild(Node* node) {
    leftChild = node;
}

Node* Node::getLeftChild() {
    return leftChild;
}

void Node::addRightChild(Node *node) {
    rightChild = node;
}

Node* Node::getRightChild() {
    return rightChild;
}

void Node::addParent(Node *p) {
    parent = p;
}

Node* Node::getParent() {
    return parent;
}