#include <iostream>
#include <random>
#include <chrono>
#include "Bst.h"
#include "Avl.h"
#include "Rbt.h"

using namespace std;

void checkTab(int* tab, int len) {
    for(int i=1;i<len;i++){
        if(tab[i-1] > tab[i]) {
            cout << "Array is not sorted correctly" << endl;
            break;
        }
    }
}

void showTab(int* tab, int len) {
    cout << "Tab: ";
    for(int i=0;i<len;i++){
        cout << tab[i] << " ";
    }
    cout << endl << endl;
}

void findDups(int* tab, int len) {
    int dupsLen = len/2 +1;
    int dups[dupsLen];
    int last = -1;

    for(int i=1;i<len;i++) {
        if(tab[i - 1] == tab[i]) {
            if(last == -1) {
                dups[++last] = tab[i];
            } else {
                if(dups[last] != tab[i]) {
                    dups[++last] = tab[i];
                }
            }
        }
    }

    cout << "Duplicates: ";

    for(int i=0;i<=last;i++) {
        cout << dups[i] << " ";
    }

    cout << endl << endl;
}

int main() {
    mt19937 mt(chrono::steady_clock::now().time_since_epoch().count());
    uniform_real_distribution<double> dist(1.0, 40.0);

    int len = 20;
    int* tab = new int[len];

    for(int i=0;i<len;i++){
        int temp = dist(mt);
        int j=0;
        bool duplicate = false;
        while(j<=i and !duplicate) {
            if(temp == tab[j]) {
                duplicate = true;
                break;
            }
            j++;
        }
        if(duplicate) {
            i--;
        } else {
            tab[i] = temp;
        }
    }

    showTab(tab, len);

    Rbt rbt = Rbt();
    for(int i=0;i<len;i++){
        rbt.addVal(tab[i]);
    }

    rbt.inorder(rbt.getRoot());

    uniform_real_distribution<double> dist2(1.0, double(len));
    int duplicatesTab [len/3];
    bool duplicate = false;
    for(int i=0;i<len/3;i++) {
        int temp = dist2(mt);
        for(int j=0;j<i;j++) {
            if(duplicatesTab[j] == temp) {
                duplicate = true;
                break;
            }
        }
        if(duplicate) {
            i--;
            duplicate = false;
        } else {
            cout << endl << "Node to delete: " << tab[temp];
            duplicatesTab[i] = temp;
            rbt.deleteKey(tab[temp]);
        }
    }

    cout << endl << endl;

    rbt.inorder(rbt.getRoot());
}