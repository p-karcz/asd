#ifndef ASD_RBT_H
#define ASD_RBT_H

#include "Node.h"
#include "Bst.h"

class Rbt: public Bst {
private:
    void swapColors(Node* n1, Node* n2);
    Node* minimum(Node* node);
    void rbTransplant(Node* u, Node* v);
    void deleteFix(Node* node);
public:
    Rbt();
    Rbt(int root);
    Node* addVal(int val);
    Node* deleteKey(int key);
    void fixColoring(Node* node);
};

#endif