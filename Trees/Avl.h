#ifndef ASD_AVL_H
#define ASD_AVL_H
#include "Bst.h"

class Avl: public Bst {
public:
    Avl();
    Avl(int root);
    Node* addVal(int val) override;
    Node* deleteKey(int key) override;
    int getBalance(Node* node);
};

#endif