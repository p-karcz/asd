#include "Avl.h"

Avl::Avl(){}

Avl::Avl(int root): Bst(root){}

Node* Avl::addVal(int val) {
    Node* node = Bst::addVal(val);
    root->updateHeight();

    while(node != nullptr) {

        if(node->getHeightDifference() > 1) {
            if(val < node->getLeftChild()->getValue()) {
                rightRotation(node);
            } else if(val > node->getLeftChild()->getValue()) {
                leftRotation(node->getLeftChild());
                rightRotation(node);
            }
            root->updateHeight();
            break;
        } else if(node->getHeightDifference() < -1) {
            if(val < node->getRightChild()->getValue()) {
                rightRotation(node->getRightChild());
                leftRotation(node);
            } else if(val > node->getRightChild()->getValue()) {
                leftRotation(node);
            }
            root->updateHeight();
            break;
        }

        node = node->getParent();
    }

    return node;
}

Node* Avl::deleteKey(int key) {
    Node* node = Bst::deleteKey(key);
    root->updateHeight();

    while(node != nullptr) {
        if(node->getHeightDifference() > 1) {
            if(node->getLeftChild()->getHeightDifference() >= 0) {
                rightRotation(node);
            } else if(node->getLeftChild()->getHeightDifference() < 0) {
                leftRotation(node->getLeftChild());
                rightRotation(node);
            }

            root->updateHeight();
            break;
        } else if(node->getHeightDifference() < -1) {
            if(node->getRightChild()->getHeightDifference() <= 0) {
                leftRotation(node);
            } else if(node->getRightChild()->getHeightDifference() > 0) {
                rightRotation(node->getRightChild());
                leftRotation(node);
            }

            root->updateHeight();
            break;
        }

        node = node->getParent();
    }

    return node;
}

int Avl::getBalance(Node *node) {
    if(node == nullptr) {
        return 0;
    } else {
        return node->getHeightDifference();
    }
}
