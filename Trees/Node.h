#ifndef ASD_NODE_H
#define ASD_NODE_H

class Node {
private:
    int value;
    int height;
    bool colorBlack;
    Node* leftChild;
    Node* rightChild;
    Node* parent;
public:
    Node();
    Node(int val, Node* p);
    Node(int val, int height, Node* p);
    Node(int val, bool isBlack, Node* p);
    void setValue(int val);
    int getValue();
    void setHeight(int height);
    int getHeight();
    int updateHeight();
    int getHeightDifference();
    void setColor(bool isBlack);
    bool isColoredBlack();
    void addLeftChild(Node* node);
    Node* getLeftChild();
    void addRightChild(Node* node);
    Node* getRightChild();
    void addParent(Node* p);
    Node* getParent();
};


#endif
