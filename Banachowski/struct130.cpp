#include "struct130.h"

struct130::struct130() {
    headOfList = nullptr;
    headOfStack = nullptr;
}

void struct130::push(int val) {
    Node* node = new Node(val);
    if(headOfList == nullptr) {
        headOfList = node;
    } else {
        node->next = headOfList;
        headOfList = node;
    }

    if(headOfStack == nullptr) {
        TableNode* tabNode = new TableNode(node);
        headOfStack = tabNode;
    } else if(headOfStack->val->val > val) {
        TableNode* tabNode = new TableNode(node);
        tabNode->next = headOfStack;
        headOfStack = tabNode;
    }
}

int struct130::pop() {
    int temp = headOfList->val;
    if(headOfStack->val->val == temp) {
        Node* nodeToBeDeleted = headOfList;
        headOfList = headOfList->next;
        TableNode* tabNodeToBeDeleted = headOfStack;
        headOfStack = headOfStack->next;
        delete(nodeToBeDeleted);
        delete(tabNodeToBeDeleted);
    } else {
        Node* nodeToBeDeleted = headOfList;
        headOfList = headOfList->next;
        delete(nodeToBeDeleted);
    }
    return temp;
}

void struct130::upToMin() {
    if(headOfList != nullptr) {
        Node* nodeToBeDeleted = headOfList;
        int valToBeDeleted = headOfStack->val->val;
        headOfList = headOfStack->val->next;
        headOfStack = headOfStack->next;

        Node* nextNode = headOfList->next;

        //Deleting takes linear time
        while(nodeToBeDeleted->val != valToBeDeleted) {
            delete(nodeToBeDeleted);
            nodeToBeDeleted = nextNode;
            if(nextNode != nullptr) {
                nextNode = nextNode->next;
            }
        }
    }
}