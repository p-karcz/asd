#ifndef ASD_STRUCT129_H
#define ASD_STRUCT129_H
#include "Node.h"
#include "TableNode.h"


class struct129 {
private:
    int n;
    Node* head;
    TableNode** tab;

    void empty();
public:
    struct129();
    struct129(int n);
    void push(int x);
    int pop();
    bool search(int x);
    void deleteVal(int x);
};


#endif
