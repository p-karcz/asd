#include "TableNode.h"

TableNode::TableNode() {
    val = nullptr;
    next = nullptr;
}

TableNode::TableNode(Node *node) {
    val = node;
    next = nullptr;
}