#ifndef ASD_TABLENODE_H
#define ASD_TABLENODE_H
#include "Node.h"


class TableNode {
public:
    Node* val;
    TableNode* next;

    TableNode();
    TableNode(Node* node);
};


#endif
