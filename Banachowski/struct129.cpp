#include "struct129.h"

struct129::struct129() {
    n = 100;
    head = nullptr;
    tab = new TableNode*[n];
    empty();
}

struct129::struct129(int n) {
    this->n = n;
    head = nullptr;
    tab = new TableNode*[n];
    empty();
}

void struct129::empty() {
    for(int i=0;i<n;i++) {
        tab[i] = nullptr;
    }
}

void struct129::push(int x) {
    Node* node = new Node(x);
    if(head != nullptr) {
        node->next = head;
        head->prev = node;
    }
    head = node;

    TableNode* tabNode = new TableNode(node);
    if(tab[x%n] != nullptr) {
        tabNode->next = tab[x%n];
    }
    tab[x%n] = tabNode;
}

bool struct129::search(int x) {
    return tab[x%n] != nullptr;
}

int struct129::pop() {
    int val = head->val;
    Node* next = head->next;
    next->prev = nullptr;
    delete(head);
    head = next;

    TableNode* nextTab = tab[val%n]->next;
    delete(tab[val%n]);
    tab[val%n] = nextTab;
    return val;
}

void struct129::deleteVal(int x) {
    if(tab[x%n] != nullptr) {
        Node* node = tab[x%n]->val;
        if(node != head) {
            node->prev->next = node->next;
        }
        if(node->next != nullptr) {
            node->next->prev = node->prev;
        }
        delete(node);

        TableNode* tabNode = tab[x%n];
        tab[x%n] = tab[x%n]->next;
        delete(tabNode);
    }
}
