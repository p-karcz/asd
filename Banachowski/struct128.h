#ifndef ASD_STRUCT128_H
#define ASD_STRUCT128_H

#include <stack>

using namespace std;

class struct128 {
private:
    stack<int> stack;
    int n;
    int* tab;

    void empty();
public:
    struct128();
    struct128(int n);
    int select();
    bool search(int x);
    void insert(int x);
};


#endif
