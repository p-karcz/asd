#include "struct128.h"

struct128::struct128() {
    n = 100;
    tab = new int[n];
    empty();
}

struct128::struct128(int n) {
    this->n = n;
    tab = new int[n];
    empty();
}

void struct128::empty() {
    for(int i=0;i<n;i++) {
        tab[i] = 0;
    }
}

int struct128::select() {
    if(!stack.empty()){
        int temp = stack.top();
        tab[temp]--;
        stack.pop();
        return temp;
    } else {
        return -1;
    }
}

void struct128::insert(int x) {
    stack.push(x%n);
    tab[x%n]++;
}

bool struct128::search(int x) {
    return tab[x%n] > 0;
}