#include "Node.h"

Node::Node() {
    val = -1;
    prev = nullptr;
    next = nullptr;
}

Node::Node(int newVal) {
    val = newVal;
    prev = nullptr;
    next = nullptr;
}