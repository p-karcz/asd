#ifndef ASD_NODE_H
#define ASD_NODE_H


class Node {
public:
    Node* prev;
    Node* next;
    int val;

    Node();
    Node(int newVal);
};


#endif
