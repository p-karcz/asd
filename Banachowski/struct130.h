#ifndef ASD_STRUCT130_H
#define ASD_STRUCT130_H

#include "Node.h"
#include "TableNode.h"

class struct130 {
private:
    Node* headOfList;
    TableNode* headOfStack;
public:
    struct130();
    void push(int val);
    int pop();
    void upToMin();
};


#endif
